#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2017 AJ Heller.
#
# Implemented from `Java's Condition example <http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/locks/Condition.html>`
#
"""Using a Condition to control sequencing between workers.
"""
#end_pymotw_header
import multiprocessing
import time
import logging

class SynchronizedBuffer(object):

    def __init__(self, max):
        self.lock = multiprocessing.RLock()
        self.notFull = multiprocessing.Condition(self.lock)
        self.notEmpty = multiprocessing.Condition(self.lock)

        self.manager = multiprocessing.Manager()
        self.buffer = self.manager.list()
        self.bufferMax = max


    def put(self, o):
        name = multiprocessing.current_process().name
        with self.lock:
            while len(self.buffer) == self.bufferMax:
                print('  %s: notFull.wait()' % name)
                self.notFull.wait()
                print('  %s: notFull notified' % name)
            self.buffer.append(o)
            self.notEmpty.notify()

    def take(self):
        name = multiprocessing.current_process().name
        with self.lock:
            while len(self.buffer) == 0:
                print('  %s: notEmpty.wait()' % name)
                self.notEmpty.wait()
                print('  %s: notEmpty notified' % name)
            o = self.buffer.pop()
            self.notFull.notify()
            return o, name


def put_worker(items, sbuff):
    name = multiprocessing.current_process().name
    print 'Starting', name
    for i in items:
        sbuff.put(i)

def take_worker(sbuff, n):
    name = multiprocessing.current_process().name
    print 'Starting', name
    for i in xrange(n):
        print sbuff.take()
            
        
if __name__ == '__main__':    
    sbuff = SynchronizedBuffer(3)
    
    puter = multiprocessing.Process(name='puter', target=put_worker, args=([1,3,5,7,9,0,2,4,6,8], sbuff))
    takers = [
        multiprocessing.Process(name='taker[%d]' % i, target=take_worker, args=(sbuff, 10))
        for i in range(3)
        ]

    for t in takers:
        t.start()
        time.sleep(0.5)
    puter.start()

    puter.join()
    for t in takers:
        t.join()
